# Dockerfile

This repository contains Dockerfiles for various containers that 56K.Cloud is
using in various projects.

## List of images

- [aws-docker](./aws-docker/)

  Multi-Arch Docker Image with AWS cli and Terraform. Built with variables
  defined in [Makefile](./Makefile).

- [aws-docker-platform-args](./aws-docker-platform-args/)

  Multi-Arch Docker Image with AWS cli and Terraform. Built using [Dockerfile
  Platform
  ARGs](https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/reference.md#automatic-platform-args-in-the-global-scope)
